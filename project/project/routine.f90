subroutine isqrt(a,n)
      integer :: n,i
      real :: a(n)
      !Vector inv square
      !Someone advised making 2 separate loops
      
      !do i=1,n; a(i) = sqrt(a(i)); end do
      !do i=1,n; a(i) = 1.0e0/a(i); end do
      
      do i=1,n; 
       a(i) = sqrt(a(i)) 
       a(i) = 1.0e0/a(i)
      end do
end subroutine
