#include <stdio.h>
#include <time.h>
#include <mkl.h>

int main(){
int n=2048;

double *A = calloc(n*n, sizeof(double));
double *x = calloc(n*n, sizeof(double));
double *b = calloc(n*n, sizeof(double));

int i, j;
//Creating a diagonally dominant matrix
for (i=0;i<n;i++){
 int sum=0;
  for(j=0; j<n; j++)
   {
   int r=random();
   if (i != j)
	{
	sum += abs(r);
	A[i*n+j] = r;
	}
  A[i*n+i]=sum;
  }
}
//Creating a random "x" matrix			
for (i=0; i<n; i++)
{
 for (j=0; j<n; j++){
 x[i*n+j] = random();
 }
}

clock_t startfactor, startsolution, endfactor, endsolution;


cblas_dgemv(CblasRowMajor, CblasTrans, n, n, 1.0, A, n, x, 1, 1, b, 1);
//Declaration of variables for dgetrf and dgetrs. Adjustments to the factorization and
//solving the linear system should be made here.
int c1, nrhs, ipiv[n], ok, LDA, LDB ;
char trans;
c1 = n;
LDA = n;
LDB = n;
nrhs = 1 ;
trans = 'N';

startfactor = clock();
dgetrf_(&c1, &c1, A, &LDA, ipiv, &ok);
endfactor = clock();
startsolution = clock();
dgetrs_(&trans, &c1, &nrhs, A, &LDA, ipiv, b, &LDB, &ok);
endsolution = clock();
double cpu_time_used, cpu_time_used2;
cpu_time_used = ((double) (endfactor-startfactor)) / CLOCKS_PER_SEC;
cpu_time_used2 = ((double) (endsolution-startsolution)) / CLOCKS_PER_SEC;

printf("Runtime: %f \n", cpu_time_used);
printf("Runtime: %f \n", cpu_time_used2);

    
return 0;

}
