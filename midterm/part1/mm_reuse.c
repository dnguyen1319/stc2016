#include <stdio.h>
#include <time.h>
#include <mkl.h>

int main (){

clock_t start, end;
double cpu_time_used;
//Timer start
start = clock();
int i, j, k;
//Matrix Size
int N=2048;
double *A = calloc(N*N, sizeof(double));
double *B = calloc(N*N, sizeof(double));
double *C = calloc(N*N, sizeof(double));
//using BLAS to multiply a matrix for me instead of hard-coding
cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, A, N, B, N, 0.0, C, N);
//Timer end
end = clock();

cpu_time_used = ((double) (end-start)) / CLOCKS_PER_SEC;

printf("Runtime: %f \n", cpu_time_used);

return 0;

}

