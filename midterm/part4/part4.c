#include <stdio.h>
#include <time.h>
#include <mkl.h>
#include <math.h>
#include <stdlib.h>


int main(){
int n=2048, i, j;

double **A = calloc(n, sizeof(double*));
for(i=0; i<n; i++)
A[i]= (double*) calloc(n, sizeof(double));

double *x = (double*)calloc(n, sizeof(double));

double *b = (double*)calloc(n, sizeof(double));

//Creating a diagonally dominant matrix
for (i=0;i<n;i++){
 int sum=0;
  for(j=0; j<n; j++)
   {
   double r = random();
   if (i != j)
	{
	sum+=r;
	A[i][j] = r;
	}
  A[i][i]=sum;
  }
}
//Creating a random "b" matrix			
for (i=0; i<n; i++)
{
 b[i] = random();
	while(b[i]=0){
	b[i] = b[i]+1;}
}

clock_t startfactor, startsolution, endfactor, endsolution;


cblas_dgemv(CblasRowMajor, CblasTrans, n, n, 1.0, *A, n, x, 1, 1, b, 1);
//Declaration of variables for dgetrf and dgetrs. Adjustments to the factorization and
//solving the linear system should be made here.
int c1, nrhs, ipiv[n], ok, LDA, LDB ;
char trans;
c1 = n;
LDA = n;
LDB = n;
nrhs = 1 ;
trans = 'N';

startfactor = clock();
dgetrf_(&c1, &c1, *A, &LDA, ipiv, &ok);
endfactor = clock();
startsolution = clock();
dgetrs_(&trans, &c1, &nrhs, *A, &LDA, ipiv, b, &LDB, &ok);
endsolution = clock();
double cpu_time_used, cpu_time_used2;
cpu_time_used = ((double) (endfactor-startfactor)) / CLOCKS_PER_SEC;
cpu_time_used2 = ((double) (endsolution-startsolution)) / CLOCKS_PER_SEC;

printf("Runtime: %f \n", cpu_time_used);
printf("Runtime: %f \n", cpu_time_used2);

//Solving with Jacobi's iterative method
int k, itr, maxit, e, sum;
double relError, temp, big, sigma;
e = 0.00001;
itr = 10000;
double *dx = (double*)calloc(n, sizeof(double));

for(i=0;i<n;i++){
dx[i]=1;
x[i]=1;
}
startsolution = clock();
for(k=0;k<=itr;k++){
	for(i=0;i<n;i++){
	sigma = 0;
		for(j=0;j<n;j++){
			if(j!=i){
			sigma = sigma + A[i][j]*dx[j];
			}
		}
	dx[i]=(b[i]-sigma)/A[i][i];
	}
	relError = fabs(x[k]-dx[k])/fabs(x[k]);
	if(relError<=e){
	printf("Successful in %d iterations\n", k);
	endsolution = clock();
	cpu_time_used2 = ((double) (endsolution-startsolution)) / CLOCKS_PER_SEC;
	printf("Runtime: %F \n",cpu_time_used2);
	exit(1);
	}
}
return 0;

}
