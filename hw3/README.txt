2000x2000	mm_noreuse	mm_reuse
icc -O0		55.70 s		55.45 s
gcc -O0		55.37 s		55.64 s
icc		1.79 s		60.32 s
gcc		55.37 s		55.64 s
icc -O3		1.87 s		29.88 s
gcc -O3		15.38 s		15.33 s

2048x2048
icc -O0		62.39 s		60.34 s		
gcc -O0		60.35 s		60.66 s
icc		1.88 s		65.40 s
gcc		60.37 s		60.67 s
icc -O3		2.00 s		33.44 s
gcc -O3		16.69 s		16.61 s
