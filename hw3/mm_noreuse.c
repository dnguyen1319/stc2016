#include <stdio.h>
#include <time.h>

int main (){

clock_t start, end;
double cpu_time_used;

start = clock();
int i, j, k;
//Matrix Size
int N=2048;
double A[N][N], B[N][N], C[N][N];

	for (i=0; i < N; i++)
	{
		for (j=0; j < N; j++)
		{	
		C[i][j]=0;
			for (k=0; k< N; k++)
			{	
			C[i][j] = C[i][j] + A[i][k] * B[k][j];
		
			}	
	
		}
	}

end = clock();

cpu_time_used = ((double) (end-start)) / CLOCKS_PER_SEC;

printf("Runtime: %f \n", cpu_time_used);

return 0;

}
